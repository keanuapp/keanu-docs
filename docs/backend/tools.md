# Backend Tools Setup

To setup, run, and maintain a Keanu backend deployment using our pre-built
automation, you will need to install and configure a few tools and software
dependencies.

These tools need to be available on the workstation(s) used by the
administrator to setup and maintain the Keanu deployment.

1. [Terraform](https://www.terraform.io/downloads.html) - The automation tool
   used to interact with AWS.
2. [aws-vault](https://github.com/99designs/aws-vault) -  A vault for securely
   storing and accessing AWS credentials on a workstation
3. [Mozilla's sops](https://github.com/mozilla/sops) - encrypts password, keys, and other
   secrets securely so they can be committed to git.
4. [terraform-provider-sops](https://github.com/carlpett/terraform-provider-sops) -
   A Terraform plugin for using files encrypted with Mozilla sops.
5. [GNU stow](https://www.gnu.org/software/stow/) - a symlink manager, used
   under the scenes by Keanu's automation
6. make and golang

## Debian Setup

On a Debian based distro you can get going quickly by following this section.

These instructions were tested on Debian 9 Stretch, your mileage on other
distros may vary.

### 1. Install from APT

Some, but not all of the tools you need are available in the apt repos
directly.

```bash
sudo apt update
sudo apt install stow make dirmngr unzip git golang-1.8
```

### 2. Prepare home environment

#### Setup path for local binaries

For the tools that aren't available in apt, we will install them into a
location in our home directory and add this location to our PATH.

If you already home such a setup or your own convention for installing
non-packaged software, skip this step, but be sure to alter the following steps
accordingly.

```bash
cd ~
mkdir -p .local/bin
echo 'export PATH="~/.local/bin:$PATH"' >> ~/.bashrc
source ~/.bashrc
```

#### Setup GOPATH

```bash
cd ~
echo 'export GOPATH=~/.local/go' >> ~/.bashrc
echo 'export PATH="/usr/lib/go-1.8/bin:$PATH"' >> ~/.bashrc
echo 'export PATH="$(go env GOPATH)/bin:$PATH"' >> ~/.bashrc
source ~/.bashrc
```

Verify go insallation is version 1.8 or greater:
```console
$ go version
go version go1.8.1 linux/amd64
```

### 3. Install Terraform


#### Install Hashicorp's PGP key

You can find Hashicorp's PGP key ID at https://www.hashicorp.com/security.html

```bash
gpg  --keyserver=hkp://pool.sks-keyservers.net --recv HASHICORP_KEY_ID
```

#### Fetch the Terraform binary and signature files

We will install the latest version of Terraform from the [Terraform downloads
page](https://www.terraform.io/downloads.html).

```
cd ~/.local/bin
export TERRAFORM_LATEST_VERSION=0.xx.xx
wget https://releases.hashicorp.com/terraform/${TERRAFORM_LATEST_VERSION}/terraform_${TERRAFORM_LATEST_VERSION}_linux_amd64.zip
wget https://releases.hashicorp.com/terraform/${TERRAFORM_LATEST_VERSION}/terraform_${TERRAFORM_LATEST_VERSION}_SHA256SUMS
wget https://releases.hashicorp.com/terraform/${TERRAFORM_LATEST_VERSION}/terraform_${TERRAFORM_LATEST_VERSION}_SHA256SUMS.sig
```

#### Verify the Terraform hashes

```console
$ gpg --verify  terraform_${TERRAFORM_LATEST_VERSION}_SHA256SUMS.sig terraform_${TERRAFORM_LATEST_VERSION}_SHA256SUMS
gpg: Signature made Tue 12 Mar 2019 07:19:21 PM GMT
gpg:                using RSA key 51852D87348FFC4C
gpg: /home/vagrant/.gnupg/trustdb.gpg: trustdb created
gpg: Good signature from "HashiCorp Security <security@hashicorp.com>"
[unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 91A6 E7F8 5D05 C656 30BE  F189 5185 2D87 348F FC4C
```

Note, the line `gpg: Good signature from "HashiCorp Security
<security@hashicorp.com>` is what we want to see.

#### Verify the binary matches the hashes

```console
$ sha256sum --ignore-missing -c terraform_${TERRAFORM_LATEST_VERSION}_SHA256SUMS
terraform_0.11.13_linux_amd64.zip: OK
```


#### Extract the binary and cleanup

```bash
unzip terraform_${TERRAFORM_LATEST_VERSION}_linux_amd64.zip
rm terraform*{zip,SHA256SUMS,sig}
```

#### Test Terraform installation

If you've set it up correctly, then the `terraform` command will be available
to you:

```console
$ terraform version
Terraform v0.11.13
```

### 4. Install aws-vault

aws-vault is a Go tool by 99designs that allows you to easily managed AWS
credentials, roles and MFA tokens from the command line while storing the
credentials securely.

Installation is easy:

```bash
go get -v github.com/99designs/aws-vault
```

Verify the installation succeeded:

```console
$ aws-vault --version
dev
```

Add the following to your `~/.bashrc`:

    AWS_VAULT_BACKEND=secret-service



### 5. Install sops and terraform-provider-sops

sops is a Go tool from Mozilla that makes it easy to edit encrypted secrets.
We will configure it in a later step.

```bash
go get -v -u go.mozilla.org/sops/cmd/sops
```

We also install terraform-provider-sops, a terraform plugin to integrate sops
and terraform.

```bash
go get -v github.com/carlpett/terraform-provider-sops
```

## Finished


!!! success "Next Step"
    Done! All the necessary tools are installed and you're ready to proceed.
    Continue to the [Setup Walkthrough](setup.md).
