# Configuration Options

All configuration files are stored encrypted in git using Mozilla's
sops to manage the encryption. To setup sops, head to the [tools
setup](tools.md) docs.

### A note on password/key generation

When a configuration option is said to be 'a long random key', you
should securely generate a long password (at miniumum 50 random
characters). Avoid using quote symbols (' or ") to prevent config
issues.

On Linux I recommend installing the `pwgen` package, and generating
them with `pwgen 50 1`. Once configured and deployed these values
should not be changed, so be sure they are long and secure passwords.


## `common.enc.yaml`

This file contains many config values that are used throughout the terraform modules.

### Naming

Every resource created on AWS is named with the namespace-environment-name
hierarchy, this allows you to run multiple deployments side by side without
conflicts. 

For example if your organization is named "ACME Inc." and you want to deploy
Keanu you might choose the following values for your _production_ instance:

* namespace: acme
* environment: production
* name: chat
* domain_name: chat.acme.com

| key                 | type   | description                                                                                                                                    |
|:--------------------|:-------|:-----------------------------------------------------------------------------------------------------------------------------------------------|
| namespace           | string | the highest level of grouping, usually your organization name                                                                                  |
| environment         | string | the environment that this deployment represents (e.g, development, production, etc)                                                            |
| name                | string | the codename for this project                                                                                                                  |
| region              | string | the region in which the keanu infrastructure will be deployed                                                                                  |

### VPC

| key                 | type   | description                                                                                                                                    |
|:--------------------|:-------|:-----------------------------------------------------------------------------------------------------------------------------------------------|
| availability_zone_1 | string | the primary availability zone in which the infrastructure will be deployed                                                                     |
| availability_zone_2 | string | the secondary availability zone used as a fallback (note: automatic failover is not yet implemented, but you must define this key nonetheless) |
| cidr_block          | string | the entire ip address block that will be used for the VPC                                                                                      |
| subnet_public_1     | string | a subnet inside cidr_block that will be used as the first public subnet (public means it is externally accessible)                             |
| subnet_public_2     | string | a subnet inside cidr_block that will be used as the second public subnet (public means it is externally accessible)                            |
| subnet_private_1    | string | a subnet inside cidr_block that will be used as the first private subnet (private means it is not externally accessible)                       |
| subnet_private_2    | string | a subnet inside cidr_block that will be used as the second private subnet (private means it is not externally accessible)                      |
| subnet_rds_a_1      | string | a subnet inside cidr_block that will be used to house the RDS instance                                                                         |
| subnet_rds_a_2      | string | a subnet inside cidr_block that will be used for the RDS instance in the 2nd availability zone as the failover                                 |

### Domains

There are three domains you need for a full Keanu deployment:

1. the common name, this is the primary domain that will serve your matrix homeserver. example: `chat.acme.com`
2. the push domain, this is the domain used to serve the push gateway. It *MUST* be the common name prefixed with `push.` (example: `push.chat.acme.com`)
3. the domain for the web client (riot), used to serve the riot web chat application. example: `web.chat.acme.com`

| key                | type   | description                                                                                                              |
|:-------------------|:-------|:-------------------------------------------------------------------------------------------------------------------------|
| matrix_dns_names   | string | a comma separated list of the common name and push domain
| matrix_common_name | string | a single domain, the common name of your deployment, the primary domain name that the matrix server will be available at |
| riot_dns_names     | string | a comma separated list of the domains needed by riot                                                                     |
| riot_common_name   | string | a single domain, the common name for riot                                                                                |

## `cloudflare.enc.yaml`

This file contains the credentials for your Cloudflare account.

| key                | type   | description                                                                        |
|:-------------------|:-------|:-----------------------------------------------------------------------------------|
| `cloudflare_zone`  | string | the cloudflare zone, the primary domain associated with cloudflare you want to use |
| `cloudflare_email` | string | the email address of your cloudflare account                                       |
| `cloudflare_token` | string | the API token                                                                      |


## `gitlab.enc.yaml`

| key                 | type   | description                                                      |
|:--------------------|:-------|:-----------------------------------------------------------------|
| `gitlab_project_id` | string | The gitlab project ID that will be automatically deploy riot web |
| `gitlab_token`      | string | Your Gitlab API token                                            |


## `bastion.enc.yaml`

The bastion hosts provides a single entry point into the internal VPC. Users
whose public keys are on the bastion can access the internal network.

| key                    | type   | description                                                                                                                                                                                                                                                                            |
|:-----------------------|:-------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `ssh_public_key_names` | string | a comma separated list of the key names that are added to the S3 bucket that controls SSH access. Each item on this list should have a corresponding ssh key in `_data/public_keys`. For example if you add `jane.pub` to that folder, then add `jane` to the list in the yaml config. |

## `synapse-rds.enc.yaml`

This file contains configuration keys for the RDS database instance.

| key                         | type   | description                                                                                                                              |
|:----------------------------|:-------|:-----------------------------------------------------------------------------------------------------------------------------------------|
| `database_password`         | string | a long random key, used as the password for the postgresql account. Once again, we recommend, using `pwgen` to generate a long password. |
| `rds_instance_class `       | string | The RDS instance class.                                                                                                                  |
| `rds_allocated_storage `    | string | The amount of allocated storage, in GB                                                                                                   |
| `rds_engine `               | string | The database engine.                                                                                                                     |
| `rds_engine_version `       | string | The database engine version.                                                                                                             |
| `rds_major_engine_version ` | string | Specifies the major version of the engine that this option group should be associated with                                               |
| `rds_family `               | string | The family of the DB parameter group                                                                                                     |

The values prefixed with `rds_` are passed to the terraform rds module,
[documentation here](https://www.terraform.io/docs/providers/aws/r/db_instance.html#attributes-reference) and the also in the [terraform-aws-modules/terraform-aws-rds](https://github.com/terraform-aws-modules/terraform-aws-rds).

## `acme-account.enc.yaml` / `acme-account-staging.enc.yaml`

These two files contain the settings for the Let's Encrypt accounts,
one for production use and one for staging.

In this case staging and production do *not* refer to your matrix
deployment, but instead the Let's Encrypt API servers.

Let's Encrypt has [weekly
limits](https://letsencrypt.org/docs/rate-limits/) on the number of
certificates you can issue, so if you are doing some heavy testing we
recommend you use the staging API server and then switch to production
when you are ready.

Both of these files have the same configuration keys:

| key          | type   | description                                                                                                                                                                      |
|:-------------|:-------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `server_url` | string | the ACME api server to use, we recommend leaving the default                                                                                                                     |
| `email`      | string | the email address you want to associate with the LE account, this should be a sysadmin email or a shared organization email as certificate expiration notices will be sent there |
| `key`        | string | a 4096 private RSA key, generate it with `openssl genrsa 4096`. Make sure to indent it with 4 spaces.                                                                   |

## `tls-private-keys.enc.yaml` / `tls-private-keys-staging.enc.yaml`

In this config file are the TLS private keys used to secure the HTTPS
connections. You need one key for matrix, the identity server and the push
gateway; and a second for the riot web client. In total you need two pairs, one
for Let's Encrypt production and staging.

We recommend generating ECDSA curves of type P256. You can generate a key using openssl: 


```bash
openssl ecparam -name prime256v1 -genkey -noout
```

| key            | type   | description                                                                          |
|:---------------|:-------|:-------------------------------------------------------------------------------------|
| `matrix.key`   | string | the private TLS key used for the matrix homeserver, identity server and push gateway |
| `matrix.algo`  | string | the key's algorithim                                                                 |
| `matrix.curve` | string | the curve used to generate the key, if the algo is ECDSA                             |
| `riot.key`     | string | the private TLS key used for the riot web client                                     |
| `riot.algo`    | string | the key's algorithim                                                                 |
| `riot.curve`   | string | the curve used to generate the key, if the algo is ECDSA                             |


## `matrix-stack.enc.yaml`

### synapse configuration

The following are keys that are used to configure synapse. Read more
about them in the synapse [sample config](https://github.com/matrix-org/synapse/blob/master/docs/sample_config.yaml)
docs.

| key                          | type    | description                                                                                                                                                                                                                                                                                                                                                                              |
|:-----------------------------|:--------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `macaroon_secret_key`        | string  | a long random key                                                                                                                                                                                                                                                                                                                                                                        |
| `password_config_pepper`     | string  | a long random key                                                                                                                                                                                                                                                                                                                                                                        |
| `shared_registration_secret` | string  | a long random key                                                                                                                                                                                                                                                                                                                                                                        |
| `form_secret`                | string  | a long random key                                                                                                                                                                                                                                                                                                                                                                        |
| `synapse_signing_key`        | string  | This final value is the signing key used by the matrix encryption protocol. You can generate it using a little python script we developed for this purpose: [generate-signing-key.py](https://gist.github.com/abeluck/bb7b28fa75872212839e6fb3f07000d3). Make sure and include the entire output string in the config, example `ed25519 a_xxxx NTXP21xxxxxx/xxxxxxxxxxxxxxxxxxxYQSKqMWI` |
| `ami_synapse`                | string  | An ID for your synapse AMI                                                                                                                                                                                                                                                                                                                                                               |
| `instance_type_synapse`      | string  | An EC2 instance type that your synapse server will use                                                                                                                                                                                                                                                                                                                                   |
| `synapse_disk_allocation_gb` | integer | The amount, in GB, that the local EBS volume will be provisioned with on the synapse EC2 instance                                                                                                                                                                                                                                                                                                                                                                                         |

### mxisd

The SMTP credentials for the mxisd identity server email connector are optional.

[More reading at the mxisd project](https://github.com/kamax-matrix/mxisd/blob/master/docs/threepids/medium/email/smtp-connector.md)

| key                    | type   | description                                                                   |
|:-----------------------|:-------|:------------------------------------------------------------------------------|
| `mxisd_email_host`     | string | the hostname of your SMTP server                                              |
| `mxisd_email_port`     | string | the port of your SMTP server                                                  |
| `mxisd_email_login`    | string | the username of your SMTP server                                              |
| `mxisd_email_password` | string | a long random key, the password configured on your SMTP host                  |
| `mxisd_email_from`     | string | the email address that will appear in the From header when mxisd sends emails |
| `ami_mxisd`            | string | The ID for your mxisd AMI                                                     |
| `instance_type_mxisd`  | string | An EC2 instance type that your mxisd will use                                 |


### sygnal


| key                    | type   | description                                                                   |
|:-----------------------|:-------|:------------------------------------------------------------------------------|
| `ami_sygnal`            | string | The ID for your sygnal AMI                                                     |
| `instance_type_sygnal`  | string | An EC2 instance type that your sygnal will use                                 |


## Push Server 

### `ios.enc.yml`

This file contains the Apple APNS certificate and package ids for your iOS
applications and enables the matrix server to send push notifications to your
users.

The format of this file consists of a top level `app` key, which is a YAML
dictionary of values containing json fragments. The json fragments must contain the key/values:

* `package`: The push identifier your app uses, must be different between sandbox and prod versions.
* `platform`:  either `sandbox` or `prod`
* `key`: A PEM formatted certificate and private key with all linefeeds replaced
  with `\n` (a helper script for this is included at
  `keanu-terraform-modules/scripts/pem-to-json.py`)

### `android.enc.yml`

TODO
