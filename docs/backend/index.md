# Keanu Platform Backend

Welcome to Keanu Platform Backend reference architecture.

Keanu is powered by the [Matrix](https://matrix.org) ecosystem, a secure,
decentralized communications network.

This documentation is your starting point to setup a Keanu deployment to AWS.
The architecture defined here is aimed at medium to large deployments of Keanu with
hundreds to thousands of users. There is no reason this configuration couldn't
be used for smaller communities, but the AWS costs involved might make it
impractical. The monthly AWS costs of an out-of-the-box deployment is roughly
estimated at 300 USD.

The intended audience of this project is systems administrators/devops folk.
The setup process is well documented but requires familiarly with command line,
linux tooling and AWS.

The canonical link for this documentation is
[https://docs.keanu.im/backend](https://docs.keanu.im/backend), and the source
is available at [keanuapp/keanu-docs](https://gitlab.com/keanuapp/keanu-docs).

## High Level Overview

The Keanu backend is composed of several servers and a PostgresSQL database
deployed to AWS, with supplementary services provided by Cloudflare and Gitlab
CI. The deployment is automated with terraform and at no point do you need to
make changes in the AWS console, with the exception of setting up your AWS
account.

The infrastructure is defined as code (with terraform) and is committed to git.
All runtime secrets (keys, passwords, api credentials etc) are also committed to
git, but encrypted with [mozilla/sops](https://github.com/mozilla/sops).

You will clone the
[keanuapp/keanu-reference-deployment](https://gitlab.com/keanuapp/keanu-reference-deployment)
project as your starting point to setup your own infrastructure. This project
contains minimal bootstrapping configuration needed to utilize our terraform
modules.

The heavy lifting is done by our terraform modules in
[keanuapp/keanu-terraform-modules](https://gitlab.com/keanuapp/keanu-terraform-modules).
This repository is versioned, thanks to git, and releases are made in such a
way that you can upgrade to known tested and working configurations.

We use an opinionated approach to managing and securing AWS accounts and
resources. It is best to start fresh with a new AWS account, though this is not
absolutely necessary (more on this later on).

The approach uses multiple accounts to segregate deployment environments.
Child accounts are below the root account, there is one for each "environment"
loosely defined ("production", "staging", etc)

To access these accounts you must do what AWS calls "role switching". In linux
terms, this is sort of like a "su otheruser".

The reason for this is so that we can ensure development, testing, production,
or even multiple production resources are separate from one another. This is a
security measure, to prevent a breach in one account from affecting other
infrastructure. It is also possible to have groups of administrators with
access to different infrastructure. Another advantage is that the billing of
each environment is broken out for cost control.

Accounts:

* *root* - required - This is the primary AWS account, the root of the Organization. It
  creates the child accounts, is where admin users login, and has consolidated
  billing for all child accounts.
* *production* - Where your primary Keanu service offering will run. You could
  even run multiple production environments if you want multiple separate offerings.
* *staging* - Where you run your testing and QA
* *development* - Where your admins and developers can actively work on prod-like infrastructure

Each account has its own terraform state backend stored in one S3 bucket.

## System Architecture

The Keanu backend is deployed to AWS using a standard multi-tiered
architecture.

The client/user connects over HTTPS to an application load balancer, which
splits the requests based on the intended destination.

There are three EC2 instances that run the server apps needed:

1. *synapse* - the primary matrix app server that handles event traffic
2. *mxisd* - the identity server that manages 3PIDs
3. *sygnal* - the push notification gateway used to proxy push notifications to
   Apple's APNS or Google's GCM

These servers are in a private VPC subnet that is not directly accessible to
the internet. The servers egress traffic happens through an AWS Internet
Gateway (with NAT).

The instances are provisioned using an AMI (disk image) that is pre-baked with
the necessary software. Guardian Project builds the AMIs and makes them
available to the public, however the tooling to build the AMIs yourself is
available here TODO.

Configuration of the applications (certs, keys, db passwords, etc) that are not
stored in the database are injected at runtime via the AWS Systems Manager
Parameter Store (SSM).

Synapse and mxisd connect to a Postgresql database hosted on AWS RDS. The
database is in a separate private subnet and Security Group rules are used to
permit synapse and mxisd to connect to it. The RDS server is a fully managed
database instance. Backups and snapshots as well as maintenance once upgrade are
handled automatically by AWS.

There is no direct access to the internal VPC, this includes ssh. To get a shell
on the EC2 instances, AWS' [Session
Manager](https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager.html)
is used.


All of the above is deployed in a single Availability Zone (AZ) in one AWS
region. In the event of an AWS data server/service outage in the AZ, the Keanu
service will be disrupted.  Future efforts will see us expanding availability
and redundancy by utilizing additional AZs.

The DNS records are managed automatically as part of the automation using
Cloudflare and terraform.

[![diagram of the aws architecture described above](aws-diagram-720.png)](aws-diagram.png)

!!! success "Next Step"
    Get started by following the [Tools Setup Guide](tools.md) on your admin workstation.
