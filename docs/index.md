# Keanu - Resilient Messaging on Matrix

Keanu is a platform for encrypted by default built-in censorship resistance enhanced on-device security offline, nearby communication and sharing 


Powered by [![matrix.org](images/matrix-logo-xs.png)](https://matrix.org) the open network for secure, decentralized communication.[matrix.org].

[Source code](https://gitlab.com/keanuapp)


!!! info
    This documentation is under rapid development.


## Get Going

1. Set up your [Keanu Backend Platform](backend/index.md)
